#include <iostream>
#include <string>
#include <vector>
#include <memory>

#include <ctime>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "common.h"
#include "motiondetector.h"

int main(int argc, char *argv[])
{
    const char* argKeys = "{file | | Input file to use} {webcam  | 0 | Webcam index } {threshold | 30 | Motion threshold }";
    cv::CommandLineParser parser(argc, argv, argKeys);

    std::string classifier = "";

    std::unique_ptr<MotionDetector> mDetector;
    if(argc > 1)
    {
	/*
        if(parser.has("file"))
        {
            cv::String fileName = parser.get<cv::String>("file");
            mDetector = std::unique_ptr<MotionDetector>{new MotionDetector{fileName}};
        }
        else if(parser.has("webcam"))
        {
            int webcamNum = parser.get<int>("webcam");
            mDetector = std::unique_ptr<MotionDetector>{new MotionDetector{webcamNum}};

        }
        int motionThresh = parser.get<int>("threshold");
        cout << motionThresh << endl;
        if(!parser.check())
        {
            parser.printErrors();
            return 1;
        }
	*/
        classifier = argv[1];

        mDetector = std::unique_ptr<MotionDetector>{new MotionDetector{0, classifier}};
    }
    else if(argc > 2)
    {
        mDetector = std::unique_ptr<MotionDetector>{new MotionDetector{argv[1], classifier}};
    }	
    mDetector->setMotionThreshold(30);
    mDetector->openCaptureDevice();
    mDetector->setShouldDisplayFrames(true);
    mDetector->processAllInputFrames();
        
    return 0;	
}
