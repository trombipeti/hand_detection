#!/bin/bash

method=$(zenity --list --text "Source of the image" --radiolist --column "Select" --column "Image source" TRUE "File on disk" FALSE "Webcam");
filename=webcam

case $method in
"File on disk")
    filename=$(zenity --file-selection --file-filter="Image files | *.jpg *.png");
    ;;
"")
    echo "Bye";
    exit 0;
    ;;
*)
    ;;
esac


./hand_detection_rpi $filename
