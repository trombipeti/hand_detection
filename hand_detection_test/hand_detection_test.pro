
QT       += core gui opengl
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TEMPLATE = app
CONFIG += c++11 link_pkgconfig
PKGCONFIG += opencv
INCLUDEPATH += ../src cqtopencvviewergl

QMAKE_CXXFLAGS += -std=c++11 -Wextra
QMAKE_LFLAGS += -lGL -lGLU

SOURCES += main.cpp \
    mainwindow.cpp \
    ../src/common.cpp \
    ../src/skincolormodel.cpp \
    ../src/shapeclassifier.cpp \
    ../src/fourierdescriptors.cpp \
    ../src/videoprocessor.cpp \
    ../src/motiondetector.cpp \
    ../src/qpiclabel.cpp \
    cqtopencvviewergl/cqtopencvviewergl.cpp

FORMS += \
    mainwindow.ui

HEADERS += \
    mainwindow.h \
    ../src/common.h \
    ../src/skincolormodel.h \
    ../src/shapeclassifier.h \
    ../src/fourierdescriptors.h \
    ../src/videoprocessor.h \
    ../src/motiondetector.h \
    ../src/qpiclabel.h \
    ../cqtopencvviewergl/cqtopencvviewergl.h \
    cqtopencvviewergl/cqtopencvviewergl.h
