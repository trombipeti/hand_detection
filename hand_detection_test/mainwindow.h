#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include <string>
#include <memory>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

#include "motiondetector.h"

namespace Ui {
class MainWindow;
}

using namespace cv;
using std::string;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_actionOpen_picture_triggered();

    void on_btnRun_clicked();

    void on_actionGet_picture_from_webcam_triggered();

    void on_paramSpinBox_valueChanged(int arg1);

    void on_methodCombo_currentIndexChanged(int index);

    void on_actionOpen_video_triggered();

    void on_actionSet_object_classifier_triggered();

private:
    Ui::MainWindow *ui;

    string m_imgName;
    Mat m_img;

    string m_videoName;
    string m_ObjClassifierFile;
    std::unique_ptr<MotionDetector> m_motionDetector;

    bool m_videoProcessing;

    void runSelectedMethod();
};

#endif // MAINWINDOW_H
