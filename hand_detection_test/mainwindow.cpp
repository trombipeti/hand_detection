#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <iostream>
#include <iomanip>
#include <map>
#include <sstream>

#include <QString>
#include <QFileDialog>
#include <QComboBox>

#include <opencv2/imgproc/imgproc.hpp>

#include "common.h"
#include "skincolormodel.h"

enum Method { SKIN_COLOR, SHAPE_DETECTION, OBJECT_DETECTION, WEBCAM_OBJECT_DETECTION };
vector<string> methods { "Skin color", "Shape detection", "Object detection", "Webcam object detection" };

//--------------------------------------------------------------------------------------------------
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    for(string& m : methods)
    {
        ui->methodCombo->addItem(m.c_str());
    }
    ui->methodCombo->setCurrentIndex(SHAPE_DETECTION);
    m_videoName = "";
    m_videoProcessing = false;
}
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
MainWindow::~MainWindow()
{
    delete ui;
}
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
void MainWindow::on_actionOpen_picture_triggered()
{
    QString imgName = QFileDialog::getOpenFileName(0, "Open picture", QString(),
                                                   "Picture files (*.jpg *.JPG *.jpeg *.png *.PNG)");

    if(! imgName.isEmpty())
    {
        m_imgName = imgName.toStdString();
        m_img = imread(m_imgName);
        if(m_img.empty())
        {
            ui->statusbar->showMessage(QString("Could not load ") + imgName);
        }
        else
        {
            ui->cvViewer->showImage(m_img);
//            showCVImage(m_imgName, m_img);
        }
    }
}
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
void MainWindow::on_btnRun_clicked()
{
    runSelectedMethod();
}
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
void MainWindow::on_actionGet_picture_from_webcam_triggered()
{
    VideoCapture vidCap{0};

    if(vidCap.isOpened())
    {
        Mat frame;

        vidCap >> frame;

        cv::resize(frame, m_img, cv::Size(640, 480));

//        m_imgName = "WebCam1";

        ui->cvViewer->showImage(m_img);
//        showCVImage(m_imgName, m_img);

        vidCap.release();
    }
    else
    {
        ui->statusbar->showMessage("Could not open webcam");
    }
}
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
void MainWindow::runSelectedMethod()
{
    Mat resultImg;
    int method = ui->methodCombo->currentIndex();
    bool motionDetection = false;

    switch(method)
    {
    case SKIN_COLOR:
        if(m_img.empty())
        {
            ui->statusbar->showMessage("No picture loaded");
        }
        else
        {
            testSkinColor(m_img, resultImg);
            ui->resultcvView->showImage(resultImg);
        }
        break;
    case SHAPE_DETECTION:
        if(m_img.empty())
        {
            ui->statusbar->showMessage("No picture loaded");
        }
        else
        {
//            testSkinColor(m_img, resultImg);
            testShapeDetection(m_img, resultImg, ui->paramSpinBox->value());
            ui->resultcvView->showImage(resultImg);
        }
        break;
    case OBJECT_DETECTION:
        if(m_videoName == "")
        {
            ui->statusbar->showMessage("No video loaded");
        }
        else
        {
            if(m_motionDetector.get() == nullptr)
            {
                m_motionDetector = std::unique_ptr<MotionDetector>{new MotionDetector{m_videoName, m_ObjClassifierFile}};
            }
            motionDetection = true;
        }
        break;
    case WEBCAM_OBJECT_DETECTION:
        if(m_motionDetector.get() == nullptr)
        {
            m_motionDetector = std::unique_ptr<MotionDetector>{new MotionDetector{0, m_ObjClassifierFile}};
        }
        ui->statusbar->showMessage("Using webcam 0 as default");
        motionDetection = true;
        break;

    default:
        ui->statusbar->showMessage("Method unimplemented");
        break;
    }

    if(motionDetection)
    {
        if(m_videoProcessing == false)
        {
            m_motionDetector->setMotionThreshold(ui->paramSpinBox->value());
            m_motionDetector->start();

            ui->btnRun->setText("Stop");
            ui->methodCombo->setEnabled(false);
            m_videoProcessing = true;
        }
        else
        {
            if(m_motionDetector.get())
            {
                m_motionDetector->stop();
                m_motionDetector.reset();
            }
            ui->btnRun->setText("Run");
            ui->methodCombo->setEnabled(true);
            m_videoProcessing = false;
        }
    }
}
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
void MainWindow::on_paramSpinBox_valueChanged(int arg1)
{
    if(m_videoProcessing)
    {
        m_motionDetector->setMotionThreshold(arg1);
    }

    if(ui->methodCombo->currentIndex() == SHAPE_DETECTION)
    {
        runSelectedMethod();
    }
}
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
void MainWindow::on_methodCombo_currentIndexChanged(int index)
{
    bool videoEnabled = false;
    if(index == OBJECT_DETECTION)
    {
        videoEnabled = true;
    }

    QList<QAction*> actions = ui->menuFile->actions();
    for(auto action: actions)
    {
        if(action->objectName() == "actionOpen_video")
        {
            action->setEnabled(videoEnabled);
        }
    }

    if(index == SHAPE_DETECTION)
    {
        ui->paramSpinBox->setMaximum(1024);
    }
}
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
void MainWindow::on_actionOpen_video_triggered()
{
    QString videoName = QFileDialog::getOpenFileName(0, "Open video", QString(),
                                                   "Video files (*.avi *.AVI *.mp4 *.webm *.mkv)");

    if(!videoName.isEmpty())
    {
        m_videoName = videoName.toStdString();
    }
}
//--------------------------------------------------------------------------------------------------

void MainWindow::on_actionSet_object_classifier_triggered()
{
    QString classifier = QFileDialog::getOpenFileName(0, "Open classifier XML", QString(),
                                                   "XML files (*.xml *.XML)");

    if(!classifier.isEmpty())
    {
        m_ObjClassifierFile = classifier.toStdString();
    }
}
