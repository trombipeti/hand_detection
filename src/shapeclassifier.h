#ifndef SHAPECLASSIFIER_H
#define SHAPECLASSIFIER_H

#include <vector>

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "common.h"

using ShapeClass = int;

class ShapeClassifier
{
private:
    int m_coeffNum;

    Mat m_refDescriptors;
public:
    explicit ShapeClassifier(const string& savedFileName = "");

    double getHandDiffSSD(Contour const& contour);
    int coeffNum() const;
};

#endif // SHAPECLASSIFIER_H
