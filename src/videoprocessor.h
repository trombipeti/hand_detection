#ifndef VIDEOPROCESSOR_H
#define VIDEOPROCESSOR_H

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <thread>
#include <atomic>
#include <functional>

/**
 * @brief The VideoProcessor class provides an abstract base class/interface for processing an abritrary video stream
 *         frame by frame. It uses OpenCV to read the input frames.
 *
 * To make use of this class, one must override the processOneFrame function, and maybe write a custom
 * constructor.
 *
 * <b>Usage</b>
 *
 * The usage of the interface is very simple:
 *
 *  - After initiating an instance, the start() function starts a thread that processes every frame read from the input
 * stream one after another. If no more frames can be read, it stops. \n
 *  - To stop this thread manually, the stop() function can be used. \n
 *  - To determine if the processing thread is running, one should use the isRunning() function. \n
 */
class VideoProcessor
{
private:

    /**
     * @brief m_stopSignal  Atomic boolean for signaling the thread.
     *
     * Using the built-in atomic type ensures that there will be no problems with out-of-order writes.
     * One has to use the m_captureMutex mutex for reading/writing this variable.
     */
    volatile std::atomic_bool m_stopSignal;

    /**
     * @brief m_videoCapture An instance of the VideoCapture object used for reading from the input.
     *
     * It will be initiated by the openCaptureDevice() function.
     */
    cv::VideoCapture m_videoCapture;

    /**
     * @brief m_deviceIndex Index of the video device to read from. If < 0, m_videoFile is used.
     */
    int m_deviceIndex;

    /**
     * @brief m_videoFile The name of the video file to read. If empty, m_deviceIndex is used.
     */
    std::string m_videoFile;

    /**
     * @brief m_realTimeProcessing  If true, we try to be realtime in the processing thread.
     * This is only used if we're processing a video file, as FPS from a webcam cannot be read by OpenCV.
     */
    bool m_realTimeProcessing;

    /**
     * @brief m_isProcessThreadRunning Stores the state of the processing thread.
     *
     * It is set to true at the very beginning of the processing thread, and set to false at the very end.
     * No need to use mutexes to read as it is atomic.
     */
    volatile std::atomic_bool m_isProcessThreadRunning;

    /**
     * @brief processOneFrame Abstract function that is called every time a frame is read from the input.
     * @param rawFrame The last frame from the input. It is guaranteed not to be empty.
     */
    virtual void processOneFrame(const cv::Mat& rawFrame) = 0;

    /**
     * @brief m_captureThread The object storing the processing thread.
     */
    std::thread m_captureThread;

protected:

    /**
     * @brief initState Initializes the internal variables. This should be called in every other constructor.
     */
    void initState()
    {
        m_stopSignal = false;
        m_isProcessThreadRunning = false;
    }


public:

    /**
     * @brief VideoProcessor Constructs the object to read from the given video file.
     *
     * This function needs the compiler to support C++11!
     *
     * @param fileName The name of the file to read frames from.
     *
     * @param realTime  If true, the processing thread will try to match the video FPS. This means that it will wait
     *                  for a certain time after every frame.
     *                  If the processOneFrame() function takes more time than one frame, it has no effect.
     */
    VideoProcessor(const std::string& fileName, bool realTime = false)
        : m_deviceIndex{-1}, m_videoFile{fileName}, m_realTimeProcessing{realTime}
    {
        initState();
    }

    /**
     * @brief VideoProcessor Constructs the object to read from the given device.
     *
     * @param deviceIndex   Index of the device to use. If in doubt, pass 0.
     */
    VideoProcessor(int deviceIndex) : m_deviceIndex{deviceIndex}
    {
        initState();
    }

    /** @cond HIDDEN */
    /** @internal No move constructor because of internal std::thread object. */
    VideoProcessor(VideoProcessor&& rhs) = delete;
    /** @internal No move operator= because of internal std::thread object. */
    VideoProcessor& operator=(VideoProcessor&& rhs) = delete;
    /** @internal No copy constructor because of internal std::thread object. */
    VideoProcessor(const VideoProcessor&) = delete;
    /** @internal No copy operator= because of internal std::thread object. */
    VideoProcessor& operator=(const VideoProcessor&) = delete;
    /** @endcond */

    /**
     * @brief ~VideoProcessor
     *
     * Stops the processing thread and waits for it to finish.
     */
    virtual ~VideoProcessor();

    /**
     * @brief processAllInputFrames  Reads frames from m_videoCapture until an empty frame is received
     * (or a stop signal, in case it is run on a seperate thread), and passes the frame to processOneFrame().
     */
    virtual void processAllInputFrames();

    /**
     * @brief isRunning Tells whether the input processing thread is running.
     *                  You can use this function to check if the thread really stopped execution after a previous
     *                  call to stop(), or to check if all the input frames have been processed.
     *
     * @return If true, it means the the processing thread will attempt to read at least one more frame and process it.
     *          If false, the no further reads will be performed. This can either mean that stop() was called before,
     *          or that there are no more frames in the input stream.
     */
    bool isRunning() const
    {
        return m_isProcessThreadRunning;
    }

    /**
     * @brief openCaptureDevice Opens the device to capture frames from it. This method must only be called if you are
     * not using the threading functionlaity of this class.
     * @return
     */
    bool openCaptureDevice();

    /**
     * @brief start Starts the input video stream processing thread.
     */
    void start();

    /**
     * @brief stop  Signals the processing thread - if running - to stop execution.
     *
     * After calling this function, the processing thread reads and processes at most one more frame, and then stops.
     * It is safe to call this function if the thread isn't running - though it is somewhat pointless to do.
     */
    void stop()
    {
        m_stopSignal = true;
        if(m_captureThread.joinable())
        {
            m_captureThread.join();
        }
    }
};

#endif // VIDEOPROCESSOR_H
