#include "videoprocessor.h"

#include <chrono>
#include <ctime>
#include <iostream>
#include <thread>

using std::cout;
using std::endl;

//--------------------------------------------------------------------------------------------------
void VideoProcessor::start()
{
    if(m_isProcessThreadRunning)
    {
        return;
    }
    m_stopSignal = false;

    if(!openCaptureDevice())
    {
        return;
    }

    if(m_captureThread.joinable())
    {
        m_captureThread.join();
    }
    m_captureThread = std::thread(&VideoProcessor::processAllInputFrames, this);
}
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
void VideoProcessor::processAllInputFrames()
{
    m_isProcessThreadRunning = true;

    double videoFps = m_videoCapture.get(CV_CAP_PROP_FPS); // for webcam this is rubbish, but we don't care anyways :)

    double secCounter = 0;

    unsigned long frameTimeMs = (videoFps != 0 ? 1000.0 / videoFps : 0);
    unsigned long frameCounter = 0;
    cv::Mat frameRaw;
    while(true)
    {
        // Check if we have to stop before doing anything.
        if(m_stopSignal)
        {
            break;
        }


        std::clock_t capStart = std::clock();

        m_videoCapture >> frameRaw;
        if(frameRaw.empty())
        {
            break;
        }

        processOneFrame(frameRaw);
        ++frameCounter;

        std::clock_t capEnd = std::clock();


        double elapsedMs = 1000.0 * ((double)(capEnd - capStart) / (double)CLOCKS_PER_SEC);
        cout << "Elapsed: " << elapsedMs << endl;

        secCounter += elapsedMs;
        double fps = frameCounter / (secCounter / 1000.0);
        if(secCounter >= 1000)
        {
            secCounter = 0;
            frameCounter = 0;
        }

        cout << "FPS: " << fps << endl;

        double toSleepMs = frameTimeMs - elapsedMs;
        if(m_deviceIndex == -1 && toSleepMs > 0)
        {
//            std::cout << "Sleep: " << toSleepMs << std::endl;
#if DISPLAY_WITH_OPENCV
            int keyCode = cv::waitKey((int)(toSleepMs + 1.0));
            if((keyCode & 0xFF) == ' ')
            {
                while((cv::waitKey() & 0xFF) != ' ')
                    ;
            }
            else if((keyCode & 0xFF) == 27) // ESC
            {
                m_stopSignal = true;
            }

#else
             std::this_thread::sleep_for(std::chrono::duration<double, std::milli>{toSleepMs});
#endif
        }
#if DISPLAY_WITH_OPENCV
        else
        {
            int keyCode = cv::waitKey(1);
            if((keyCode & 0xFF) == ' ')
            {
                while((cv::waitKey() & 0xFF) != ' ')
                    ;
            }
            else if((keyCode & 0xFF) == 27) // ESC
            {
                m_stopSignal = true;
            }
        }
#endif

    }

    m_isProcessThreadRunning = false;
}
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
VideoProcessor::~VideoProcessor()
{
    stop();
}
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
bool VideoProcessor::openCaptureDevice()
{
    if(!m_videoFile.empty())
    {
        m_videoCapture = cv::VideoCapture{m_videoFile};
    }
    else if(m_deviceIndex >= 0)
    {
        m_videoCapture = cv::VideoCapture{m_deviceIndex};
    }
    else
    {
        return false;
    }
    return true;
}
//--------------------------------------------------------------------------------------------------
