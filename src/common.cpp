#include "common.h"

#include <assert.h>
#include <fstream>
#include <sstream>
#include <ctime>
#include <iomanip>

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "fourierdescriptors.h"
#include "skincolormodel.h"
#include "shapeclassifier.h"


using std::string;
using namespace cv;

//--------------------------------------------------------------------------------------------------
void showCVImage(const string& winName, const Mat& image)
{
    namedWindow(winName, CV_WINDOW_NORMAL);
    imshow(winName, image);
}
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
void testSkinColor(const Mat& img, Mat& result)
{
    Mat imgConverted{img.size(), CV_8UC3};
    cvtColor(img, imgConverted, CV_BGR2YCrCb);

    result = Mat::zeros(img.size(), CV_8UC3);

    getSkinColorMask(imgConverted, result);

    //    showCVImage("Skin color mask", result);
}
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
void saveDefectsToFile(const vector<Vec4i>& defects, const vector<Point>& contour, const Size& imgSize)
{
    time_t rawTime;
    struct tm* timeinfo;
    char* localTimeString;
    std::stringstream filename;

    time(&rawTime);
    timeinfo = std::localtime(&rawTime);
    localTimeString = std::asctime(timeinfo);
    localTimeString[strlen(localTimeString) - 1] = '\0'; // Get rid of the stupid newline

    filename << "../train/" << localTimeString << ".dat";
    cout << "Filename: " << filename.str() << endl;

    std::ofstream ofs(filename.str().c_str());

    cout << "[ Size ]" << endl;
    ofs << "[ Size ]" << endl;

    cout << imgSize.width << "x" << imgSize.height << endl;
    ofs << imgSize.width << "x" << imgSize.height << endl;

    cout << "[ Contour ]" << endl;
    ofs << "[ Contour ]" << endl;
    for (size_t i = 0; i < contour.size(); ++i)
    {
        cout << contour[i].x << "," << contour[i].y;
        ofs << contour[i].x << "," << contour[i].y;
        if (i < contour.size() - 1)
        {
            cout << ";";
            ofs << ";";
        }
    }
    cout << endl;
    ofs << endl;

    cout << "[ Defects ]" << endl;
    ofs << "[ Defects ]" << endl;

    for (Vec4i defect : defects)
    {
        for (size_t i = 0; i < 4; ++i)
        {
            cout << defect[i];
            ofs << defect[i];
            if (i < 3)
            {
                cout << ";";
                ofs << ";";
            }
        }
        cout << endl;
        ofs << endl;
    }
    ofs.close();
}
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
void drawConvHullWithDefects(const Mat& src, Mat& result, vector<vector<Point>>& contours, size_t maxIndex)
{
    result = src.clone();
    if (result.channels() == 1)
    {
        cvtColor(result, result, CV_GRAY2BGR);
    }

    vector<vector<Point>> convHull(1);
    vector<vector<int>> convHullIdx(1);
    convexHull(contours[maxIndex], convHull[0]);
    convexHull(contours[maxIndex], convHullIdx[0], false, false);

    for (size_t i = 0; i < contours.size(); ++i)
    {
        drawContours(result, contours, i, Scalar(255, i * 10 % 255, i * 10 % 255), 2);
    }

    drawContours(result, contours, maxIndex, Scalar(255, 200, 40), result.cols / 100);
    drawContours(result, convHull, 0, Scalar(255, 30, 240), result.cols / 100);

    if (convHullIdx[0].size() > 3)
    {
        vector<vector<Vec4i>> convDefects(1);
        vector<Vec4i>         filterDefects;
        convexityDefects(contours[maxIndex], convHullIdx[0], convDefects[0]);
        for (size_t i = 0; i < convDefects[0].size(); ++i)
        {
            const Vec4i& defect = convDefects[0][i];
            float depth = defect[3] / 256.0;

            if (depth > 15)
            {
                int startIdx = defect[0];
                Point ptStart(contours[maxIndex][startIdx]);
                int endIdx = defect[1];
                Point ptEnd(contours[maxIndex][endIdx]);
                int farIdx = defect[2];
                Point ptFar(contours[maxIndex][farIdx]);

                Point ptMiddle{ (ptStart.x + ptEnd.x) / 2, (ptStart.y + ptEnd.y) / 2 };

                // A lehető legkevesebb castolás, ami ahhoz kell, hogy ne warningoljon
                Vec2d a{(double)ptStart.x - ptFar.x, (double)ptStart.y - ptFar.y};
                Vec2d b{(double)ptEnd.x - ptFar.x,   (double)ptEnd.y - ptFar.y};

                a = normalize(a);
                b = normalize(b);

                // Háromszög szöge > 10° -> megtartjuk
                //                    cout << a[0] << "," << a[1] << " * " << b[0] << "," << b[1] << " = " << a.ddot(b) << endl;
                //                    cout << "SZÖG: " << acos(a.ddot(b)) * M_PI/180.0 << endl;
                if (acos(a.ddot(b)) > (10 / 180.0) * M_PI)
                {
                    filterDefects.push_back(defect);
                    line(result, ptStart, ptEnd, Scalar(0, 255, 0), 2);
                    line(result, ptStart, ptFar, Scalar(0, 255, 0), 2);
                    line(result, ptEnd, ptFar, Scalar(0, 255, 0), 2);
                    line(result, ptMiddle, ptFar, Scalar(0, 100, 200), 1);
                    circle(result, ptFar, 4, Scalar(0, 255, 255), 1);
                    circle(result, ptStart, 4, Scalar(0, 255, 255), 1);
                }
            }
        }
    }

    showCVImage("Convex hull", result);
}
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
void testShapeDetection(const Mat& img, Mat& result, int maxCoeffs)
{
    //    int threshLimit = 30;

    Mat bwImg;
    if (img.channels() == 3)
    {
        cvtColor(img, bwImg, CV_BGR2GRAY);
        threshold(bwImg, bwImg, 0, 255, CV_THRESH_BINARY_INV | CV_THRESH_OTSU);
    }
    else if (img.channels() == 1)
    {
        bwImg = img.clone();
        threshold(img, bwImg, 0, 255, CV_THRESH_BINARY_INV | CV_THRESH_OTSU);
    }
    else
    {
        std::cout << "Unsupported channel size: " << img.channels() << std::endl;
        return;
    }

    Contour largestContour;
    int largestIndex = findLargestContour(bwImg, largestContour);

    if (largestIndex >= 0)
    {

        int maxCoeffsLimited = std::min((int)largestContour.size(), maxCoeffs);

        Mat descriptors, dNorm;
        Point2f center;
        computeFourierDescriptors(largestContour, dNorm, maxCoeffsLimited, center, true);
        computeFourierDescriptors(largestContour, descriptors, maxCoeffsLimited, center, false);

        drawFourierDescriptorChart(dNorm);

//        drawConvHullWithDefects(img, result, contours, maxIndex);
        drawShapeFromFourierDescriptors(img, descriptors, 0, maxCoeffs, center, result);

        std::stringstream coeffStr;
        coeffStr << maxCoeffs;
        Size textSize = getTextSize(coeffStr.str(), FONT_HERSHEY_DUPLEX, 2.0, 2, 0);
        putText(result, coeffStr.str(), Point(10, textSize.height + 10), FONT_HERSHEY_DUPLEX, 2.0, Scalar(100, 10, 255), 2 );

        showCVImage("Shape from descriptors", result);
    }
    else
    {
        cout << "No contours found" << endl;
    }
}
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
Rect operator *(const Rect& r, double scale)
{
    Rect result = r;
    result *= scale;
    return result;

}
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
Rect& operator *=(Rect& r, double scale)
{
    Point2f center{(float)r.x + r.width * 0.5f, (float)r.y + r.height * 0.5f};
    double newWidth = r.width * scale;
    double newHeight = r.height * scale;

    r.x = center.x - newWidth * 0.5;
    r.y = center.y - newHeight * 0.5;
    r.width = newWidth;
    r.height = newHeight;
    return r;
}
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
Rect scaleRectInsideImage(const Rect& r, double scale, const Mat& img)
{
    Rect result = r * scale;
    if (result.x < 0) // "underflow"
    {
        result.width += result.x; // Itt negatív az x! :)
        result.x = 0;
    }
    if ((result.x + result.width) > (img.cols - 1)) // "overflow"
    {
        result.width -= ((result.x + result.width) - (img.cols - 1)); // Amennyivel túllóg
        result.x = std::max(img.cols - result.width, 0);
    }

    // Same for Y
    if (result.y < 0)
    {
        result.height += result.y; // Y is negatív itten! :)
        result.y = 0;
    }
    if ((result.y + result.height) > (img.rows - 1))
    {
        result.height -= ((result.y + result.height) - (img.rows - 1)); // Amennyivel túllóg
        result.y = std::max(img.rows - result.height, 0);
    }

    if (result.height == 0 || result.width == 0)
    {
        result = r;
    }

    return result;
}
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
int findLargestContour(const Mat& img, Contour& largestContour)
{
    if (img.channels() != 1)
    {
        cout << "findLargestContour only works on 1-channel images!" << endl;
        return -1;
    }

    int result = -1;
    vector<Contour> contours;
    vector<int> indices = findLargestNContours(img, contours, 1);
    if(indices.size() > 0)
    {
        largestContour = contours[0];
        result = indices[0];
    }
    return result;
}
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
vector<int> findLargestNContours(Mat img, vector<Contour> &contours, int n)
{
    vector<int> result;
    if (img.channels() != 1)
    {
        cout << "findLargestNContours only works on 1-channel images!" << endl;
        return result;
    }

    vector<Contour> allContours;
    findContours(img, allContours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);

    if(allContours.size() > 0)
    {
        std::sort(allContours.begin(), allContours.end(),
                  [](vector<Point> const & a, vector<Point> const & b)
                    {
                        return contourArea(a) > contourArea(b);
                    }
        );

        if(n == -1 || n > (int)allContours.size())
        {
            n = allContours.size();
        }

        for(int i = 0;i<n;++i)
        {
            contours.push_back(allContours[i]);
            result.push_back(i);
        }
    }
    return result;

}
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
void skeleton(const Mat &src, Mat &dst)
{
    Mat kern = getStructuringElement(CV_SHAPE_CROSS, Size(3,3));
    dst = Mat::zeros(src.size(), CV_8UC1);

    Mat img = src.clone();
    while(countNonZero(img) > 0)
    {
        Mat tmp, eroded;
        erode(img, eroded, kern);
        dilate(eroded, tmp, kern);
        subtract(img, tmp, tmp);
        bitwise_or(dst, tmp, dst);
        img = eroded.clone();
    }

}
//--------------------------------------------------------------------------------------------------
