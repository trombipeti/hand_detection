#include "fourierdescriptors.h"

#include <iostream>

#include <opencv2/imgproc/imgproc.hpp>

//--------------------------------------------------------------------------------------------------
void computeFourierDescriptors(Contour const& contour, Mat& result, size_t numCoeffs, bool normalize)
{
    Point2f center;
    computeFourierDescriptors(contour, result, numCoeffs, center, normalize);
}
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
void computeFourierDescriptors(Contour const& contour, Mat& result, size_t numCoeffs, Point2f& massCenter, bool normalize)
{
    Moments contourMoment = cv::moments(contour);

    massCenter = Point2f( contourMoment.m10 / contourMoment.m00,
                                  contourMoment.m01 / contourMoment.m00 );

    Mat contourMat{ Size(contour.size(), 2), CV_32F };
    for(size_t i = 0;i<contour.size(); ++i)
    {
        contourMat.at<float>(0, i) = contour.at(i).x - massCenter.x;
        contourMat.at<float>(1, i) = contour.at(i).y - massCenter.y;
    }

    int optSize = getOptimalDFTSize(std::max(contour.size(), numCoeffs));
//    cout << "Opt size: " << optSize << endl;

    resize(contourMat, contourMat, Size(optSize, 2), 0, 0, INTER_LINEAR);

    Mat contourDFT(Size(optSize, 2), CV_32F);
    dft(contourMat, contourDFT, cv::DFT_REAL_OUTPUT);

    result = Mat::zeros(2, optSize, CV_32F);

    for(size_t i = 0;i < numCoeffs; ++i)
    {
        if(normalize)
        {
            if(i == 0)
            {
                result.at<float>(0, i) = 0;
                result.at<float>(1, i) = 0;
            }
            else
            {
                result.at<float>(0, i) = contourDFT.at<float>(0, i) / contourDFT.at<float>(0, 1);
                result.at<float>(1, i) = contourDFT.at<float>(1, i) / contourDFT.at<float>(1, 1);
            }
        }
        else
        {
            result.at<float>(0, i) = contourDFT.at<float>(0, i);
            result.at<float>(1, i) = contourDFT.at<float>(1, i);
        }
    }

//    cout << result.cols << " x " << result.rows << endl;
}
//--------------------------------------------------------------------------------------------------


//--------------------------------------------------------------------------------------------------
void drawFourierDescriptorChart(Mat const& descriptors)
{
    vector<double> lengths;

    Mat plane1, plane2, magn;

//    cout << planes[0].depth() << "," << planes[1].depth() << endl;

//    magnitude(plane1, plane2, magn);

    magn = descriptors.clone();

    cout << magn << endl;

    for(int i = 0;i<magn.cols; ++i)
    {
        lengths.push_back(std::abs(magn.at<float>(0, i)));
    }

    int lastNotNull = 0;
    for(int i = magn.cols - 1;i >= 0;--i)
    {
        if(magn.at<float>(0, i) > 0)
        {
            lastNotNull = i;
            break;
        }
    }

    double maxLen = *(std::max_element(lengths.begin(), lengths.end()));

    Mat chart{500, 500, CV_8UC3, Scalar(0)};

    int step = 500 / (lastNotNull + 1) + 1;

    for(int i = 0;i<500 - step;i += step)
    {
        int offset = std::max(0.0, 500 - (500.0 * (double)lengths[i] / maxLen));
        if(offset < 0) offset = 0;
        Point pt1{i, 500};
        Point pt2{i + step, offset};
        cout << pt1 << "," << pt2 << "," << lengths[i] << "," << maxLen << endl;
        rectangle(chart, pt1, pt2, Scalar(200, i/2, 30), -1);
    }

    showCVImage("Fourier descriptors", chart);
}
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
void drawShapeFromFourierDescriptors(Mat const& src, Mat const& descriptors, int coeffStart, int numCoeffs, const Point2f &center, Mat& shape)
{
    Mat inverseDescriptors = Mat::zeros(descriptors.size(), descriptors.type());

    Mat usedDescriptors = Mat::zeros( descriptors.size(), CV_32F);

    for(int i = coeffStart;i<numCoeffs + coeffStart && i < descriptors.cols;++i)
    {
        usedDescriptors.at<float>(0, i) = descriptors.at<float>(0, i);
        usedDescriptors.at<float>(1, i) = descriptors.at<float>(1, i);
    }

//    // DC komponens + első 2 együttható azért legyen meg
//    if(coeffStart >= 2)
//    {
//        for(int i = 0;i<=3;++i)
//        {
//            usedDescriptors.at<float>(0, i) = descriptors.at<float>(0, i);
//            usedDescriptors.at<float>(1, i) = descriptors.at<float>(1, i);
//        }
//    }

    dft(usedDescriptors, inverseDescriptors, cv::DFT_REAL_OUTPUT | cv::DFT_INVERSE | cv::DFT_SCALE);

    shape = src.clone();

    if(shape.channels() == 1)
    {
        cvtColor(shape, shape, CV_GRAY2BGR);
    }

    for(int i = 0;i < inverseDescriptors.cols; ++i)
    {
        int nextIndex = (i + 1) % inverseDescriptors.cols;

        Point2f pt1{center.x + inverseDescriptors.at<float>(0, i),
                    center.y + inverseDescriptors.at<float>(1, i) };


        Point2f pt2{center.x + inverseDescriptors.at<float>(0, nextIndex),
                    center.y + inverseDescriptors.at<float>(1, nextIndex)};

        int colorFactor = ((double)i / (double)inverseDescriptors.cols) * 255.0;
        line(shape, pt1, pt2, Scalar(255 - colorFactor, colorFactor, 30), shape.size().width / 50);
    }

//    showCVImage("Shape from descriptors", shape);
}
//--------------------------------------------------------------------------------------------------
