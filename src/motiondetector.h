#ifndef MOTIONDETECTOR_H
#define MOTIONDETECTOR_H

#include <mutex>

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/video/video.hpp>
#include <opencv2/objdetect/objdetect.hpp>

using namespace cv;

#include "videoprocessor.h"
#include "shapeclassifier.h"

/**
 * @brief  Abstract class that calculates motion images from the images acquired via VideoProcessor.
 *
 * One should inherit from this class and possibly other classes like WebcamVideoProcessor to make a class that can
 * detect motion from any image sequence taken from an arbitrary source. Processing is done on a seperate thread.
 */
class MotionDetector : public VideoProcessor
{
private:

    /**
     * @brief Implementation of the base class' function that is called every time a new frame is read.
     *
     * This just calls updateMotionImages(frameRaw);
     *
     * @param frameRaw
     */
    void processOneFrame(const cv::Mat& frameRaw) override;

    /**
     * @brief getSignificantMotionROI
     */
    Rect getSignificantMotionROI();

    /**
     * @brief updateMotionImages Calculates the difference image and the accumulated difference image from the current
     * frame and the previous one. It also saves the recieved frame to m_prevFrame.
     * If no previous frame was recieved, it only saves the previous frame, and initializes m_accumulatedMotion to zeros
     * with frameRaw's type and size.
     *
     * @param frameRaw  The newly recieved frame. It will be cloned to m_prevFrame.
     * @param alpha     Parameter for the weighted accumulation of the motion image. It must be between 0.0 and 1.0;
     */
    void updateMotionImages(const Mat& frameRaw, double alpha = 0.5);

    /**
     * @brief m_prevFrame  The previously received frame. This will be used to calculate the difference image.
     */
    cv::Mat m_prevFrame;

    /**
     * @brief m_motionImage The difference image of the two previous frames, indication motion with high values.
     */
    cv::Mat m_motionImage;

    /**
     * @brief m_accumulatedMotion The accumulated motion image that is the sum of all the difference images calculated so far.
     */
    cv::Mat m_accumulatedMotion;

    /**
     * @brief m_imageMutex Mutex to use when accessing computed frames.
     */
    std::mutex m_imageMutex;

    /**
     * @brief m_motionThreshold Threshold to used for motion detection.
     *
     * If a pixel value in the difference image from the 2 previous frames are above this value,
     * then it pixel is considered to be 'moving'.
     */
    int m_motionThreshold = 0;

    /**
     * @brief m_currentRoi ROI of the current biggest motion area.
     */
    cv::Rect m_currentRoi;

    CascadeClassifier objClassifier;

    bool shouldInvertHandMask(const cv::Mat &image, const cv::Mat& mask);

    void updateCurrentMotionROI(const Rect &curROI);

    bool shouldDisplayFrames = false;

public:

    explicit MotionDetector(const std::string& fileName, const std::string& cascadeFileName = "") : VideoProcessor{fileName}
    {
        if(cascadeFileName != "")
        {
            objClassifier.load(cascadeFileName);
        }
    }

    explicit MotionDetector(int deviceIndex, const std::string& cascadeFileName = "") : VideoProcessor{deviceIndex}
    {
        if(cascadeFileName != "")
        {
            objClassifier.load(cascadeFileName);
        }
    }



    /**
     * @brief ~MotionDetector Destructor that is very important to have, otherwise runtime thread errors occur.
     *
     * If this function didn't exist or didn't call stop() explicitly, the constructor would be called in the
     * base class' scope causing pure virtual function call. That is not something you would want.
     */
    ~MotionDetector()
    {
        stop();
    }

    /**
     * @brief   Gets a read-only reference for the calculated motion image. If no frames have been recieved yet,
     * it will return an empty image.
     *
     * @return const cv::Mat
     */
    const cv::Mat& getMotionImage()
    {
        m_imageMutex.lock();
        const cv::Mat& result = m_motionImage;
        m_imageMutex.unlock();
        return result;
    }

    /**
     * @brief   Clones the motion image. This is a costly operation. This should only be used if one wants to modify
     *          it, otherwise, use getMotionImage().
     *
     * @return cv::Mat  The cloned copy of the calculated motion image. It is empty if no frames have been recieved.
     */
    cv::Mat getMotionImageClone()
    {
        return getMotionImage().clone();
    }

    /**
     * @brief setMotionThreshold Setter function for the motion threshold.
     * @param thresh    Value between 0-255 inclusive to be used as motion threshold.
     */
    void setMotionThreshold(int thresh)
    {
        if(thresh >= 0 && thresh <= 255)
        {
            m_motionThreshold = thresh;
        }
    }
    bool getShouldDisplayFrames() const;
    void setShouldDisplayFrames(bool value);
};

#endif // MOTIONDETECTOR_H
