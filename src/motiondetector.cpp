#include "motiondetector.h"

#include "common.h"
#include "fourierdescriptors.h"
#include "skincolormodel.h"

#include <unistd.h>

#include <iostream>
#include <iomanip>
#include <sstream>
#include <opencv2/opencv.hpp>

//--------------------------------------------------------------------------------------------------
#if OLD_STUFF
void MotionDetector_processOneFrame_OLD(const Mat& frameRaw)
{

    updateMotionImages(frameRaw, 0.3);

    Rect motionRoi = getSignificantMotionROI();
    updateCurrentMotionROI(motionRoi);

    Mat roiMask = Mat::zeros(frameRaw.size(), CV_8UC1);

    Mat accumBW;
    m_imageMutex.lock();
    m_accumulatedMotion.convertTo(accumBW, CV_8UC1);
    m_imageMutex.unlock();

    threshold(accumBW(m_currentRoi), roiMask(m_currentRoi), 10, 255, CV_THRESH_BINARY);

    Mat curFrameMasked;
    frameRaw.copyTo(curFrameMasked, roiMask);

    if(m_currentRoi.area() > 0)
    {

//        Mat skinMask, skinOnly;;
//        getSkinColorMaskBGR(frameRaw(m_currentRoi), skinMask);
//        frameRaw(m_currentRoi).copyTo(skinOnly, skinMask);

//        showCVImage("ROI skin mask", skinOnly);


        Mat roiEdgesX{curFrameMasked.size(), curFrameMasked.type()},
            roiEdgesY{curFrameMasked.size(), curFrameMasked.type()};
        Mat roiEdges = Mat::zeros(curFrameMasked.size(), CV_8UC1);

        Mat frameBW;
        cvtColor(frameRaw, frameBW, CV_BGR2GRAY);

//        GaussianBlur(frameBW, frameBW, Size(3,3), 0.0);

//        Sobel(frameBW(m_currentRoi), roiEdgesX, -1, 2, 0);
//        convertScaleAbs(roiEdgesX, roiEdgesX);

//        Sobel(frameBW(m_currentRoi), roiEdgesY, -1, 0, 2);
//        convertScaleAbs(roiEdgesY, roiEdgesY);

//        addWeighted(roiEdgesX, 0.5, roiEdgesY, 0.5, 0.0, roiEdges);

        Canny(frameBW(m_currentRoi), roiEdges, 50, 100);

        threshold(roiEdges, roiEdges, 15, 255, CV_THRESH_BINARY);

//        dilate(roiEdges, roiEdges, kern);

        Mat roiEdgesBW;
//        cvtColor(roiEdges, roiEdgesBW, CV_BGR2GRAY);

        vector<Contour> contours;
        vector<int> contourIndices = findLargestNContours(roiEdges, contours, 2);

        bool handFound = false;

        cvtColor(roiEdges, roiEdges, CV_GRAY2BGR);

        Mat contourImage = frameRaw(m_currentRoi).clone();

        for(size_t i = 0;i<contours.size();++i)
        {
            if(contourArea(contours[i]) > 100)
            {
                Mat desc;
                Point2f center;
                computeFourierDescriptors(contours[i], desc, 64, center);
                drawShapeFromFourierDescriptors(contourImage, desc, 0, 64, center, contourImage);
//                drawContours(contourImage, contours, contourIndices[i], Scalar(255, (i*50) % 255, 100), 1);

//                double ssd = m_shapeClassifier.getHandDiffSSD(contours[i]);

                Rect handRect = boundingRect(contours[i]);
                rectangle(contourImage, handRect, Scalar(0, 255, 100), 2);
//                std::stringstream ssdStr;
//                ssdStr << std::setprecision(2) << ssd;
//                putText(contourImage, ssdStr.str(), handRect.tl(), FONT_HERSHEY_DUPLEX, 1.0, Scalar(255, 255, 180), 2);
            }
        }

#if 0
        vector<Contour> secondPassContours;
        vector<int> secondPassIndices = findLargestNContours(contourImage, secondPassContours, 3);
//        contourImage = Scalar(0);
        cvtColor(contourImage, contourImage, CV_GRAY2BGR);
        for(size_t i = 0;i<secondPassContours.size();++i)
        {
            if(m_shapeClassifier.classifyShape(secondPassContours[i]) > 0)
            {
                cout << "HAND" << endl;
//                Rect handRect = boundingRect(secondPassContours[i]);
//                showCVImage("Hand", frameRaw(handRect));
            }
//            computeFourierDescriptors(secondPassContours[i], desc, 64, center);
//            drawShapeFromFourierDescriptors(contourImage, desc, 0, 64, center, contourImage);
            drawContours(contourImage, secondPassContours, secondPassIndices[i], Scalar(i*50 % 255, (i*(i + 35)) % 255, 100), 1);
        }
#endif


        Mat roiEdgesMasked;
        roiEdges.copyTo(roiEdgesMasked, roiMask(m_currentRoi));

        showCVImage("ROI edges", roiEdgesMasked);
        showCVImage("Contours", contourImage);
        showCVImage("ROI mask", roiMask(m_currentRoi));
        if(handFound)
        {
            cv::waitKey(0);
        }

    }

    Mat frameCopy;
    frameRaw.copyTo(frameCopy);

    rectangle(frameCopy, m_currentRoi, Scalar(255, 100, 100), 4);
    rectangle(frameCopy, motionRoi, Scalar(100, 100, 255), 1);
    showCVImage("Motion ROI", frameCopy);
    showCVImage("Accumulated motion", m_accumulatedMotion);
    showCVImage("Input", frameRaw);
}
#endif
//--------------------------------------------------------------------------------------------------

void MotionDetector::processOneFrame(const Mat& frameRaw)
{
    Mat frameRawResized;
    resize(frameRaw, frameRawResized, Size(0,0), 0.5, 0.5);
    updateMotionImages(frameRawResized, 0.3);

    Rect motionRoi = getSignificantMotionROI();
    updateCurrentMotionROI(motionRoi);

    Mat roiMask = Mat::zeros(frameRawResized.size(), CV_8UC1);

    Mat accumBW;
    m_imageMutex.lock();
    m_accumulatedMotion.convertTo(accumBW, CV_8UC1);
    m_imageMutex.unlock();

    threshold(accumBW(m_currentRoi), roiMask(m_currentRoi), 10, 255, CV_THRESH_BINARY);

    Mat curFrameMasked;
    frameRawResized.copyTo(curFrameMasked, roiMask);
    if(m_currentRoi.area() > 0 && !objClassifier.empty())
    {
        Mat motionArea = frameRawResized(m_currentRoi);
        vector<Rect> foundObjects;
        objClassifier.detectMultiScale(motionArea, foundObjects);
        for(Rect o : foundObjects)
        {
            rectangle(motionArea, o, Scalar(255, 100, 0), 2);
        }
        cout << "Detected objects: " << foundObjects.size() << endl;
    }
    if(shouldDisplayFrames)
    {
        showCVImage("Video", frameRawResized);
    }
}

//--------------------------------------------------------------------------------------------------
void MotionDetector::updateCurrentMotionROI(const Rect& curROI)
{
    m_imageMutex.lock();
    if(!m_motionImage.empty())
    {
        Mat motionBW;
        cvtColor(m_motionImage, motionBW, CV_BGR2GRAY);
        threshold(motionBW, motionBW, m_motionThreshold, 255, CV_THRESH_BINARY);
        if(countNonZero(motionBW) >= curROI.area() * 0.005)
        {
            Rect intersection = (curROI & m_currentRoi);
//            // Az eltárolt roi-n belül van-e a jelenleg megtalált
            bool roiInsideCurrent = (intersection == curROI);

//            // Van-e közös metszetük
            bool roiExpandsCurrent = (intersection.area() > 0);

//            // Az előző roi területének 1/X részénel nagyobb
            bool roiTooSmall = curROI.area() <= m_currentRoi.area() * 0.1;

            // Ha "kicsinyedik" a téglalap, akkor megtartjuk, amennyiben nem túl gyors ez a kicsinyedés
            // Ha van közös metszet, akkor ha nagyobb az eddigi felénél a jelenlegi ROI, megtartjuk
            // Ha nincs közös metszet, akkor csak az olyan ROI-t tartjuk meg, ami nagyobb, mint a jelenlegi
            if ((roiInsideCurrent && !roiTooSmall) ||
                (roiExpandsCurrent && curROI.area() >= m_currentRoi.area() * 0.5) ||
                (!roiInsideCurrent && curROI.area() > m_currentRoi.area()))
            {
                m_currentRoi = scaleRectInsideImage(curROI, 1.2, m_motionImage);
            }
        }
    }
    m_imageMutex.unlock();
}

bool MotionDetector::getShouldDisplayFrames() const
{
    return shouldDisplayFrames;
}

void MotionDetector::setShouldDisplayFrames(bool value)
{
    shouldDisplayFrames = value;
}
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
void MotionDetector::updateMotionImages(const Mat& frameRaw, double alpha)
{
    if (m_accumulatedMotion.empty())
    {
        m_accumulatedMotion = Mat::zeros(frameRaw.size(), CV_32FC1);
    }

    if (!m_prevFrame.empty())
    {
        Mat motionBW;

        m_imageMutex.lock();
        absdiff(m_prevFrame, frameRaw, m_motionImage);
        cvtColor(m_motionImage, motionBW, CV_BGR2GRAY);
        m_imageMutex.unlock();

        threshold(motionBW, motionBW, m_motionThreshold, 255, CV_THRESH_BINARY);

//        Mat kern = getStructuringElement(CV_SHAPE_RECT, Size(5,5));
//        dilate(motionBW, motionBW, kern);

        m_imageMutex.lock();
        accumulateWeighted(motionBW, m_accumulatedMotion, alpha);
        m_imageMutex.unlock();
    }
    m_prevFrame = frameRaw.clone();
}
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
Rect MotionDetector::getSignificantMotionROI()
{
    Rect result;
    Mat accumBW;
    m_accumulatedMotion.convertTo(accumBW, CV_8U);

    Contour largest;
    if(findLargestContour(accumBW, largest) >= 0)
    {
        result = boundingRect(largest);
    }
    return result;
}
//--------------------------------------------------------------------------------------------------

#if 0
void doMagic()
{

    // Az eltárolt roi-n belül van-e a jelenleg megtalált
    bool roiInsideCurrent = ( (roiRect & m_currentRoi) == roiRect);

    // Az előző roi területének 1/X részénel nagyobb
    bool roiTooSmall = roiRect.area() <= m_currentRoi.area() * 0.1;

    // Számottevő mozgás, jelenleg mérnöki hasraütés módszere
    bool significantMotion = contourArea(contours[maxIndex]) >= roiRect.area() * 0.05;

   roiRect = scaleRectInsideImage(roiRect, 1.5, curFrame);

    // Ha nagyon kicsi a jelenlegi mozgás területe, akkor az nem számít
    // Ha az eddigin belül van a teljes ROI, akkor azt megtartjuk
    // Ha az eddiginél nagyobb a mozgás területe, átugrunk arra
    if (significantMotion &&
        ((roiInsideCurrent && !roiTooSmall) || roiRect.area() >= m_currentRoi.area()) &&
            nonZeroMotion > 0)
    {
        m_currentRoi = roiRect;
    }

    if (m_currentRoi.area() > 0)
    {

//                rectangle(curFrame, m_currentRoi, Scalar(0, 255, 255), 2);
//                Mat motionArea;
//                Mat gradX, gradY, absGrad;
//                cvtColor(frameRaw, motionArea, CV_BGR2GRAY);
//                //            Sobel(motionArea, gradX, -1, 2, 0);
//                //            Sobel(motionArea, gradY, -1, 0, 2);
//                //            convertScaleAbs(gradX, gradX);
//                //            convertScaleAbs(gradY, gradY);
//                //            addWeighted(gradX, 0.5, gradY, 0.5, 0, absGrad);

//                Canny(motionArea, absGrad, 50, 150);

//                threshold(absGrad, absGrad, 20, 255, CV_THRESH_BINARY);
//                Mat kern = getStructuringElement(CV_SHAPE_RECT, Size(3, 3));
//                dilate(absGrad, absGrad, kern);

//                contours.clear();
//                Mat gradRoi = absGrad.clone()(m_currentRoi);
//                //            gradRoi = gradRoi(m_currentRoi);
//                findContours(gradRoi, contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);

//                largestContour = max_element(contours.begin(), contours.end(),
//                                             [](vector<Point> const & a, vector<Point> const & b)
//                {
//                    return contourArea(a) <= contourArea(b);
//                });

//                maxIndex = largestContour - contours.begin();

        Mat motionROI = curFrame(m_currentRoi).clone();

//                resize(motionROI, motionROI, Size(0, 0), 4, 4);

       Mat motionSkin;

//               cvtColor(motionROI, motionSkin, CV_BGR2YCrCb);
//               Mat skinMask;
//               getSkinColorMask(motionSkin, skinMask);

//               Mat motionSkinInv;
//               cvtColor(motionSkin, motionSkinInv, CV_YCrCb2BGR);
//               motionSkin = Scalar(0);
//               motionSkinInv.copyTo(motionSkin, skinMask);

////////////////////
         cvtColor(motionROI, motionSkin, CV_BGR2GRAY);
         threshold(motionSkin, motionSkin,    0, 255, CV_THRESH_BINARY | CV_THRESH_OTSU);
         if(shouldInvertHandMask(motionROI, motionSkin))
         {
             cout << "Inverting mask" << endl;
             bitwise_not(motionSkin, motionSkin);
         }
////////////////////

////////////////////
//                Mat skinMask;
//                cvtColor(motionROI, motionSkin, CV_BGR2YCrCb);
//                getSkinColorMask(motionSkin, skinMask);
//                motionSkin = Scalar(0,0,0);
//                motionROI.copyTo(motionSkin, skinMask);
////////////////////


        Ptr<StarDetector> detector = StarDetector::create(16);
        vector<KeyPoint> keypoints;
        detector->detect(motionSkin, keypoints);
        Mat motionKP = motionSkin.clone();
        drawKeypoints(motionROI, keypoints, motionKP, Scalar(255, 0, 100),
                        DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS | DrawMatchesFlags::DRAW_RICH_KEYPOINTS);

        showCVImage("Keypoints", motionKP);

        Mat shapeResult;
        testShapeDetection(motionSkin, shapeResult, "../train/hand.dat");
//                testShapeDetection(motionSkinInv, r, 32);
        //            drawContours(motionROI, contours, maxIndex, Scalar(0, 255, 0), 1);

        //            showCVImage("Motion ROI", motionROI);
    }

    //        showCVImage("Accum motion", accumBGR);
    if (m_currentRoi.area() > 0)
    {
//            Mat showRoi = curFrame(m_currentRoi).clone();
        rectangle(curFrame, m_currentRoi, Scalar(0, 255, 200), 2);
    }
    showCVImage("Motion area", curFrame);
}
#endif

//--------------------------------------------------------------------------------------------------
bool MotionDetector::shouldInvertHandMask(const Mat& image, const cv::Mat &mask)
{
    if(mask.cols > 2 && mask.rows > 2)
    {
//        Mat skinMask, twoMaskOverlap;
//        getSkinColorMaskBGR(image, skinMask);
//        bitwise_and(skinMask, mask, twoMaskOverlap);

//        bool notEnoughSkin = countNonZero(twoMaskOverlap) <= countNonZero(skinMask) / 2;

        Mat corners{Size(2,2), CV_8UC1};
        corners.at<uchar>(0,0) = mask.at<uchar>(0, 0);
        corners.at<uchar>(1,0) = mask.at<uchar>(mask.rows - 1, 0);
        corners.at<uchar>(0,1) = mask.at<uchar>(0, mask.cols - 1);
        corners.at<uchar>(1,1) = mask.at<uchar>(mask.rows - 1, mask.cols - 1);

        return countNonZero(corners) >= 2;// || notEnoughSkin;
    }
    return false;
}
//--------------------------------------------------------------------------------------------------
