#ifndef FOURIERDESCRIPTORS_H
#define FOURIERDESCRIPTORS_H

#include "common.h"

void computeFourierDescriptors(Contour const& contour, Mat& result, size_t numCoeffs, bool normalize = false);
void computeFourierDescriptors(Contour const& contour, Mat& result, size_t numCoeffs, Point2f& massCenter, bool normalize = false);

void drawFourierDescriptorChart(Mat const& descriptors);

void drawShapeFromFourierDescriptors(Mat const& src, Mat const& descriptors, int coeffStart, int numCoeffs, Point2f const& center, Mat &shape);

#endif // FOURIERDESCRIPTORS_H
