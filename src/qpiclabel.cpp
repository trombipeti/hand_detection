#include "qpiclabel.h"

#include <iostream>

#include <Qt>
//#include <QImage>
//#include <QResizeEvent>
//#include <QSize>
//#include <QSizePolicy>
#include <QMenu>
//#include <QPoint>
#include <QFileDialog>

#include <string>
#include <sstream>
#include <ctime>
#include <cstdlib>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

//--------------------------------------------------------------------------------------------------
QPicLabel::QPicLabel(QWidget *parent, const cv::Mat &img) :
    QLabel(parent)
{
    QSizePolicy p = sizePolicy();
    p.setHeightForWidth(true);
    setSizePolicy(p);
    std::srand(std::time(0));
    createID = std::rand();

    setContextMenuPolicy(Qt::CustomContextMenu);
    connect(this, SIGNAL(customContextMenuRequested(const QPoint&)),
        this, SLOT(showContextMenu(const QPoint&)));

    setCVImage(img);
}
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
/**
 * @brief QPicLabel::QPixmapFromCvMat Constructs a QPixmap from the given OpenCV image. Currently
 *        it supports only images with 1 and 3 channels.
 *
 * @param image Read-only reference of an OpenCV image (cv::Mat).
 *
 * @throw const char * Throws a basic message if the given image's channel number is not supported.
 *
 * @return  The newly constructed QPixmap from the given image.
 */
QPixmap QPicLabel::QPixmapFromCvMat(const cv::Mat &image) throw(const char *)
{
    QImage QIm;
    if(image.channels() == 3)
    {
        QIm = QImage((const unsigned char*)image.data, image.cols, image.rows, image.step,
                     QImage::Format_RGB888).rgbSwapped(); // OpenCV uses BGR
    }
    else if(image.channels() == 1)
    {
        QIm = QImage((const unsigned char*)image.data, image.cols, image.rows, image.step,
                     QImage::Format_Mono);
    }
    else
    {
        std::ostringstream s;
        s << "Unhandled image channel number: " << image.channels();
        throw s.str().c_str();
    }
    return QPixmap::fromImage(QIm);
}
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
/**
 * @brief setCVImage Sets the given cv::Mat as the source image for this label.
 *
 * @param image Reference of the image to be set. It is not modified, but is copied.
 */
void QPicLabel::setCVImage(const cv::Mat &image)
{
    if(!image.empty())
    {
        cvImg = image;
        int w, h;
        w = (float)(width()) * 0.95f;
        h = (float)(height()) * 0.95f;
        try
        {
            setPixmap(QPixmapFromCvMat(cvImg).scaled(w, h, Qt::KeepAspectRatio, Qt::SmoothTransformation));
        }
        catch(const char* s)
        {
            std::cerr << "QPicLabel::setPixmap: " << s << std::endl;
        }

        repaint();

        emit imageSet();
    }
}
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
void QPicLabel::resizeEvent(QResizeEvent *)
{
    const QPixmap* pm = pixmap();
    if(pm == nullptr)
    {
        return;
    }
    int w, h;
    w = (float)(width()) * 0.95f;
    h = (float)(height()) * 0.95f;


    setPixmap(QPixmapFromCvMat(cvImg).scaled(w, h, Qt::KeepAspectRatio, Qt::SmoothTransformation));
}
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
void QPicLabel::mouseDoubleClickEvent(QMouseEvent *)
{
    if(!cvImg.empty())
    {
        std::ostringstream winname;
        winname << "CV image " << createID;
        cv::namedWindow(winname.str().c_str(), CV_WINDOW_NORMAL);
        cv::imshow(winname.str().c_str(), cvImg);
    }
}
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
void QPicLabel::action_SaveImage()
{
    QString fileName = QFileDialog::getSaveFileName(0, "Save image to...", QString(),
                                                    "Image Files (*.png *.jpg *.JPG *.JPEG *.jpeg *.bmp *.tif *.ppm)");
    if(fileName.isEmpty())
    {
        return;
    }
    std::string fn = fileName.toStdString();
    if(fn.find_last_of(".") == fn.size())
    {
        fn.append("jpg");
    }

    if(fn.find_last_of(".") == std::string::npos)
    {
        fn.append(".jpg");
    }

    cv::imwrite(fn, cvImg);
}
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
void QPicLabel::action_SetImage()
{
    QString fileName = QFileDialog::getOpenFileName(0, "Open image", QString(),
                                                    "Image Files (*.png *.jpg *.JPG *.JPEG *.jpeg *.bmp *.tif *.ppm)");
    if(fileName.isEmpty())
    {
        return;
    }
    setCVImage(cv::imread(fileName.toStdString()));
}
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
void QPicLabel::action_Rotate90p()
{
    cv::transpose(cvImg, cvImg);
    cv::flip(cvImg, cvImg, 1);
    setCVImage(cvImg);
}
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
void QPicLabel::showContextMenu(const QPoint &p)
{

    QPoint posGlobal = mapToGlobal(p);
    QMenu menu;

    menu.addAction("Open picture", this, SLOT(action_SetImage()));

    if(!cvImg.empty())
    {
        menu.addAction("Save picture", this, SLOT(action_SaveImage()));
        menu.addAction("Rotate +90°", this, SLOT(action_Rotate90p()));
    }
    menu.exec(posGlobal);
}
//--------------------------------------------------------------------------------------------------
