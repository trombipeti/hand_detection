#ifndef QPICLABEL_H
#define QPICLABEL_H

#include <QLabel>
#include <QPixmap>
#include <QImage>

#include <ctime>

#include <opencv2/core/core.hpp>

class QPicLabel : public QLabel
{
    Q_OBJECT
public:
    explicit QPicLabel(QWidget *parent = 0, const cv::Mat& img = cv::Mat());

    void setCVImage(const cv::Mat& image);

    static QPixmap QPixmapFromCvMat(const cv::Mat &image) throw(const char *);

    const cv::Mat& getCVImg() const
    {
        return cvImg;
    }

private slots:
    void action_SaveImage();

    void action_SetImage();

    void action_Rotate90p();

public slots:
    void showContextMenu(const QPoint& p);

signals:
    void imageSet();

protected:
    void resizeEvent(QResizeEvent *) override;
    void mouseDoubleClickEvent(QMouseEvent *) override;

private:
    cv::Mat cvImg;
    int createID;
};

#endif // QPICLABEL_H
