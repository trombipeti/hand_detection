#ifndef SKINCOLORMODEL_H
#define SKINCOLORMODEL_H

#include <opencv2/core/core.hpp>

void getSkinColorMask(const cv::Mat& inImgYCrCb, cv::Mat& outMask);

void getSkinColorMaskBGR(cv::Mat const& inImgBGR, cv::Mat &outMask);

#endif // SKINCOLORMODEL_H
