#ifndef COMMON_H
#define COMMON_H

#include <iostream>
#include <string>
#include <vector>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

using std::cout;
using std::cin;
using std::endl;
using std::string;
using std::vector;
using namespace cv;

using Contour = std::vector<cv::Point>;

void showCVImage(const std::string& winName, const Mat &image);

vector<int> findLargestNContours(Mat img, vector<Contour> &contours, int n = 1);

int findLargestContour(const Mat &img, Contour& largestContour);

void testSkinColor(const Mat& img, Mat& result);

void testShapeDetection(const Mat& img, Mat &result, int maxCoeffs);

Rect& operator *=(Rect& r, double scale);
Rect operator *(const Rect& r, double scale);
Rect scaleRectInsideImage(const Rect& r, double scale, const Mat& img);

void skeleton(const Mat& src, Mat& dst);

#endif // COMMON_H
