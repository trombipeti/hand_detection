#include "shapeclassifier.h"

#include <iostream>
#include <fstream>

#include "fourierdescriptors.h"

//--------------------------------------------------------------------------------------------------
int ShapeClassifier::coeffNum() const
{
    return m_coeffNum;
}
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
ShapeClassifier::ShapeClassifier(const string &savedFileName)
{
    if(savedFileName != "")
    {
        std::ifstream ifs{savedFileName};
        if(!ifs.eof())
        {
            ifs >> m_coeffNum;
            m_refDescriptors = Mat::zeros(2, m_coeffNum, CV_32F);
            for(int i = 0;i<m_coeffNum;++i)
            {
                ifs >> m_refDescriptors.at<float>(0, i) >> m_refDescriptors.at<float>(1, i);
                if(ifs.eof())
                {
                    std::cerr << "Unexpected EOF" << std::endl;
                    break;
                }
            }
        }
    }
    else
    {
        m_coeffNum = 32;
        cout << "No saved state, you should train the SVM now" << endl;
    }
}
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
double ShapeClassifier::getHandDiffSSD(const Contour &contour)
{
    Mat descriptors;
    computeFourierDescriptors(contour, descriptors, m_coeffNum, true);

    double descSSD = 0;

    for(int i = m_coeffNum / 3;i<m_coeffNum;++i)
    {
        Vec2f cur{descriptors.at<float>(0,i), descriptors.at<float>(1,i)};
        Vec2f ref{m_refDescriptors.at<float>(0,i), m_refDescriptors.at<float>(1,i)};

        Vec2f diff = cur - ref;

        descSSD += diff[0]*diff[0] + diff[1]*diff[1];

//        std::cout << descriptors.at<float>(0,i) << "," << descriptors.at<float>(1,i) << std::endl;
    }

    std::cout << "SSD: " << descSSD << std::endl;


    return descSSD;
}
//--------------------------------------------------------------------------------------------------
