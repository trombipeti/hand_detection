#include "skincolormodel.h"

#include <iostream>
#include <iomanip>

#include <vector>

#include <ctime>

#include <opencv2/imgproc/imgproc.hpp>


using namespace cv;

using std::cout;
using std::cerr;
using std::endl;
using std::setw;
using std::setfill;

// Constants for spread and center computing in the transformed YCr'Cb' color space
#define W_cb 	46.97
#define WL_cb 	23
#define WH_cb 	14
#define W_cr	48.67
#define WL_cr	20
#define WH_cr	10
#define K_l     125
#define K_h     188
#define Y_min 	16
#define Y_max 	235

// Constants for the elliptical model for skin tones
#define c_x     109.38
#define c_y     152.02
#define theta   2.53    // Radians -> We need: cos, sin, -sin
#define e_cx    1.60
#define e_cy    2.41
#define a_sq    (25.39*25.39) // We need: a^2, b^2
#define b_sq    (14.03*14.03)

const double costheta  = cos(theta);
const double sintheta  = sin(theta);


//////////////////////////
// Precomputation table //
//////////////////////////

bool IsSkinTable[256][256][256] = {0};

//--------------------------------------------------------------------------------------------------
double Cb_center(double Y)
{
    double result = 0;
    if(Y < K_l)
    {
        result = 108.0 + ( (K_l - Y)*10.0 ) / (K_l - Y_min);
    }
    else if(K_h < Y)
    {
        result = 108.0 + ( (Y - K_h)*10.0 ) / (Y_max - K_h);
    }
    else
    {
//        cerr << "Cb_center is not defined for " << Y << endl;
    }
    return result;
}
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
double Cr_center(double Y)
{
    double result = 0;
    if(Y < K_l)
    {
        result = 154.0 - ( (K_l - Y)*10.0 ) / (K_l - Y_min);
    }
    else if(K_h < Y)
    {
        result = 154.0 + ( (Y - K_h)*22.0 ) / (Y_max - K_h);
    }
    else
    {
//        cerr << "Cr_center is not defined for " << Y << endl;
    }
    return result;
}
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
double Cb_spread(double Y)
{
    double result = 0;
    if(Y < K_l)
    {
        result = WL_cb + ( (Y - Y_min)*(W_cb - WL_cb) ) / (K_l - Y_min);
    }
    else if(K_h < Y)
    {
        result = WH_cb + ( (Y_max - Y)*(W_cb - WH_cb) ) / (Y_max - K_h);
    }
    else
    {
//        cerr << "Cb_spread is not defined for " << Y << endl;
    }
    return result;
}
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
double Cr_spread(double Y)
{
    double result = 0;
    if(Y < K_l)
    {
        result = WL_cr + ( (Y - Y_min)*(W_cr - WL_cr) ) / (K_l - Y_min);
    }
    else if(K_h < Y)
    {
        result = WH_cr + ( (Y_max - Y)*(W_cr - WH_cr) ) / (Y_max - K_h);
    }
    else
    {
//        cerr << "Cr_spread is not defined for " << Y << endl;
    }
    return result;
}
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
double Cr_transformed(double Y, double Cr)
{
    if(Y < K_l || K_h < Y)
    {
        double crc = Cr_center(Y);
        double crs = Cr_spread(Y);
        double khc = Cr_center(K_h);

        return (Cr - crc) * (W_cr)/(crs) + khc;
    }
    else
    {
        return Cr; // FIXME: Itt az van, hogy Ci(Y), ami nem tudom, hogy mi :(
    }
}
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
double Cb_transformed(double Y, double Cb)
{
    if(Y < K_l || K_h < Y)
    {
        double cbc = Cb_center(Y);
        double cbs = Cb_spread(Y);
        double khc = Cb_center(K_h);

        return (Cb - cbc) * (W_cb)/(cbs) + khc;
    }
    else
    {
        return Cb; // FIXME: Itt az van, hogy Ci(Y), ami nem tudom, hogy mi :(
    }
}
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
bool isSkinColor(Vec3b const& pixelYCrCb)
{

    double Y  = pixelYCrCb[0];
    double Cr = pixelYCrCb[1];
    double Cb = pixelYCrCb[2];

    double Cb_ = Cb_transformed(Y, Cb);
    double Cr_ = Cr_transformed(Y, Cr);

    double cbdiff = Cb_ - c_x;
    double crdiff = Cr_ - c_y;

    double x = ( costheta*cbdiff) + (sintheta*crdiff);
    double y = (-sintheta*cbdiff) + (costheta*crdiff);

//    cout << "x: " << x << " y: " << y << endl;

    double ell_1 = ((x - e_cx)*(x - e_cx)) / (a_sq);
    double ell_2 = ((y - e_cy)*(y - e_cy)) / (b_sq);

//    cout << "x: " << xy.at<double>(0,0);
//    cout << " y: " << xy.at<double>(1,0) << endl;

    bool inEllipse =  ell_1 + ell_2 <= 1.0;
//    bool inThresh = (138 < Cr && Cr < 178) && (200 < (Cb + Cr * 0.6) && (Cb + Cr*0.6) < 215);
    bool inThresh = (77  < Cb && Cb < 127 &&
                     137 < Cr && Cr < 177 &&
                     190 < (Cb + Cr * 0.6) && (Cb + Cr * 0.6) < 215);

    return (inEllipse || inThresh);
}
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
void computeSkinColorTable()
{
    uint y, cr, cb;
    for(y = 0;y<256;++y)
    {
        for(cr = 0;cr<256;++cr)
        {
            for(cb = 0;cb<256;++cb)
            {
                IsSkinTable[y][cr][cb] = isSkinColor(Vec3b{(uchar)y, (uchar)cr, (uchar)cb});
            }
        }
    }
}
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
/**
 * @brief getSkinColorMask  Calculates a mask for the input image, which only leaves the pixels that
 *                          fit the skin color model.
 *
 * @param inImgYCrCb        Input image to get the mask for, converted to YCrCb color space.
 *
 * @param outMask           Output binary image, containing logical 1 where the pixel in the input
 *                          image is considered to be "skin" color.
 */
void getSkinColorMask(Mat const& inImgYCrCb, Mat &outMask)
{
    static bool tableComputed = false;

    if(!tableComputed)
    {
        computeSkinColorTable();
        tableComputed = true;
    }

    if(inImgYCrCb.channels() == 3)
    {
        outMask = Mat::zeros(inImgYCrCb.size(), CV_8UC1);

        int nRows = inImgYCrCb.rows;
        int nCols = inImgYCrCb.cols;

//        clock_t startT = std::clock();

        for(int rowCnt = 0; rowCnt < nRows; ++rowCnt)
        {
            for(int colCnt = 0; colCnt < nCols; ++colCnt)
            {
                Vec3b px = inImgYCrCb.at<Vec3b>(rowCnt, colCnt);
                bool isSkin = IsSkinTable[ px[0] ][ px[1] ][ px[2] ];

                outMask.at<uchar>(rowCnt, colCnt) = (isSkin ? 255 : 0);
            }
        }

//        clock_t endT = std::clock();
//        double diff = (double)(endT - startT) / CLOCKS_PER_SEC;
//        cout << diff << "s" << endl;
//        cout << (inImgYCrCb.rows * (double)inImgYCrCb.cols) / diff << " px/s" << endl;
//        cout << "~ " << (1.0/diff) << " fps" << endl;
//        cout << "-----------------------------" << endl;
    }
}
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
void getSkinColorMaskBGR(Mat const& inImgBGR, Mat &outMask)
{
    Mat convIn;
    cvtColor(inImgBGR, convIn, CV_BGR2YCrCb);
    getSkinColorMask(convIn, outMask);
}
//--------------------------------------------------------------------------------------------------
